import random

p = 6
q = 7
mn = 100000

thefunny = {
    "hi": 1
}

def coinflip(rng):
    i = random.randint(1,10)
    if i <= rng:
        return 1
    return 0

def trial():
    # for each n week, flip a p coin to determine if Joe Lucky plays a game. 
    # if passed, flip a q coin to determine if Joe lucky wins the game.
    # increment p and q for every pass
    # at the end of the trial, record the pair of (p, q)
    # at the end of 100,000 trials, calculate the relative frequency of each (p,q) pair
    i = 0
    j = 0
    for bababooie in range(0,8):# n weeks = 8
        ligma = coinflip(p)
        i += coinflip(p)
        if ligma:
            j += coinflip(q)
    
    thefunny[(i, j)] = thefunny.get((i,j), 0) + 1

for sandwich in range(0,mn):
    trial()

print("Joint PMF of X and Y")
print("y:|   0            1             2             3             4               5              6              7              8")
print("x -----------------------------------------------------------------------------------------------------------------------------------------------")
print(f"0 | {thefunny.get((0,0), 0) / mn:0.4f}")
print(f"1 | {thefunny.get((1,0), 0) / mn:0.4f}        {thefunny.get((1,1), 0) / mn:0.4f}")
print(f"2 | {thefunny.get((2,0), 0) / mn:0.4f}        {thefunny.get((2,1), 0) / mn:0.4f}        {thefunny.get((2,2), 0) / mn:0.4f}")
print(f"3 | {thefunny.get((3,0), 0) / mn:0.4f}        {thefunny.get((3,1), 0) / mn:0.4f}        {thefunny.get((3,2), 0) / mn:0.4f}        {thefunny.get((3,3), 0) / mn:0.4f}")
print(f"4 | {thefunny.get((4,0), 0) / mn:0.4f}        {thefunny.get((4,1), 0) / mn:0.4f}        {thefunny.get((4,2), 0) / mn:0.4f}        {thefunny.get((4,3), 0) / mn:0.4f}        {thefunny.get((4,4), 0) / mn:0.4f}")
print(f"5 | {thefunny.get((5,0), 0) / mn:0.4f}        {thefunny.get((5,1), 0) / mn:0.4f}        {thefunny.get((5,2), 0) / mn:0.4f}        {thefunny.get((5,3), 0) / mn:0.4f}        {thefunny.get((5,4), 0) / mn:0.4f}        {thefunny.get((5,5), 0) / mn:0.4f}")
print(f"6 | {thefunny.get((6,0), 0) / mn:0.4f}        {thefunny.get((6,1), 0) / mn:0.4f}        {thefunny.get((6,2), 0) / mn:0.4f}        {thefunny.get((6,3), 0) / mn:0.4f}        {thefunny.get((6,4), 0) / mn:0.4f}        {thefunny.get((6,5), 0) / mn:0.4f}        {thefunny.get((6,6), 0) / mn:0.4f}")
print(f"7 | {thefunny.get((7,0), 0) / mn:0.4f}        {thefunny.get((7,1), 0) / mn:0.4f}        {thefunny.get((7,2), 0) / mn:0.4f}        {thefunny.get((7,3), 0) / mn:0.4f}        {thefunny.get((7,4), 0) / mn:0.4f}        {thefunny.get((7,5), 0) / mn:0.4f}        {thefunny.get((7,6), 0) / mn:0.4f}        {thefunny.get((7,7), 0) / mn:0.4f}")
print(f"8 | {thefunny.get((8,0), 0) / mn:0.4f}        {thefunny.get((8,1), 0) / mn:0.4f}        {thefunny.get((8,2), 0) / mn:0.4f}        {thefunny.get((8,3), 0) / mn:0.4f}        {thefunny.get((8,4), 0) / mn:0.4f}        {thefunny.get((8,5), 0) / mn:0.4f}        {thefunny.get((8,6), 0) / mn:0.4f}        {thefunny.get((8,7), 0) / mn:0.4f}        {thefunny.get((8,8), 0) / mn:0.4f}")

col = [thefunny.get((0,0), 0) + thefunny.get((1,0), 0) + thefunny.get((2,0), 0) + thefunny.get((3,0), 0) + thefunny.get((4,0), 0) + thefunny.get((5,0), 0) + thefunny.get((6,0), 0) + thefunny.get((7,0), 0) + thefunny.get((8,0), 0), thefunny.get((1,1), 0) + thefunny.get((2,1), 0) + thefunny.get((3,1), 0) + thefunny.get((4,1), 0) + thefunny.get((5,1), 0) + thefunny.get((6,1), 0) + thefunny.get((7,1), 0) + thefunny.get((8,1), 0), thefunny.get((2,2), 0) + thefunny.get((3,2), 0) + thefunny.get((4,2), 0) + thefunny.get((5,2), 0) + thefunny.get((6,2), 0) + thefunny.get((7,2), 0) + thefunny.get((8,2), 0), thefunny.get((3,3), 0) + thefunny.get((4,3), 0) + thefunny.get((5,3), 0) + thefunny.get((6,3), 0) + thefunny.get((7,3), 0) + thefunny.get((8,3), 0), thefunny.get((4,4), 0) + thefunny.get((5,4), 0) + thefunny.get((6,4), 0) + thefunny.get((7,4), 0) + thefunny.get((8,4), 0), thefunny.get((5,5), 0) + thefunny.get((6,5), 0) + thefunny.get((7,5), 0) + thefunny.get((8,5), 0), thefunny.get((6,6), 0) + thefunny.get((7,6), 0) + thefunny.get((8,6), 0), thefunny.get((7,7), 0) + thefunny.get((8,7), 0), thefunny.get((8,8), 0)]
row = ["hi", thefunny.get((8,6), 0) + thefunny.get((8,7), 0) + thefunny.get((8,8), 0) + thefunny.get((8,5), 0) + thefunny.get((8,4), 0) + thefunny.get((8,3), 0) + thefunny.get((8,2), 0) + thefunny.get((8,1), 0) + thefunny.get((8,0), 0), thefunny.get((7,7), 0) + thefunny.get((7,6), 0) + thefunny.get((7,5), 0) + thefunny.get((7,4), 0) + thefunny.get((7,3), 0) + thefunny.get((7,2), 0) + thefunny.get((7,0), 0) + thefunny.get((7,1), 0), thefunny.get((6,6), 0) + thefunny.get((6,5), 0) + thefunny.get((6,4), 0) + thefunny.get((6,3), 0) + thefunny.get((6,2), 0) + thefunny.get((6,1), 0) + thefunny.get((6,0), 0), thefunny.get((5,0), 0) + thefunny.get((5,5), 0) + thefunny.get((5,4), 0) + thefunny.get((5,3), 0) + thefunny.get((5,2), 0) + thefunny.get((5,1), 0), thefunny.get((4,4), 0) + thefunny.get((4,3), 0) + thefunny.get((4,2), 0) + thefunny.get((4,1), 0) + thefunny.get((4,0), 0), thefunny.get((3,3), 0) + thefunny.get((3,2), 0) + thefunny.get((3,1), 0) + thefunny.get((3,0), 0), thefunny.get((2,2), 0) + thefunny.get((2,1), 0) + thefunny.get((2,0), 0), thefunny.get((1,1), 0) + thefunny.get((1,0), 0), thefunny.get((0,0), 0)]

print("Conditional PMF of X given Y")
print("y:|   0            1             2             3             4               5              6              7              8")
print("x -----------------------------------------------------------------------------------------------------------------------------------------------")
print(f"0 | {thefunny.get((0,0), 0) / (col[0]):0.4f}")
print(f"1 | {thefunny.get((1,0), 0) / (col[0]):0.4f}        {thefunny.get((1,1), 0) / (col[1]):0.4f}")
print(f"2 | {thefunny.get((2,0), 0) / (col[0]):0.4f}        {thefunny.get((2,1), 0) / (col[1]):0.4f}        {thefunny.get((2,2), 0) / (col[2]):0.4f}")
print(f"3 | {thefunny.get((3,0), 0) / (col[0]):0.4f}        {thefunny.get((3,1), 0) / (col[1]):0.4f}        {thefunny.get((3,2), 0) / (col[2]):0.4f}        {thefunny.get((3,3), 0) / (col[3]):0.4f}")
print(f"4 | {thefunny.get((4,0), 0) / (col[0]):0.4f}        {thefunny.get((4,1), 0) / (col[1]):0.4f}        {thefunny.get((4,2), 0) / (col[2]):0.4f}        {thefunny.get((4,3), 0) / (col[3]):0.4f}        {thefunny.get((4,4), 0) / (col[4]):0.4f}")
print(f"5 | {thefunny.get((5,0), 0) / (col[0]):0.4f}        {thefunny.get((5,1), 0) / (col[1]):0.4f}        {thefunny.get((5,2), 0) / (col[2]):0.4f}        {thefunny.get((5,3), 0) / (col[3]):0.4f}        {thefunny.get((5,4), 0) / (col[4]):0.4f}        {thefunny.get((5,5), 0) / (col[5]):0.4f}")
print(f"6 | {thefunny.get((6,0), 0) / (col[0]):0.4f}        {thefunny.get((6,1), 0) / (col[1]):0.4f}        {thefunny.get((6,2), 0) / (col[2]):0.4f}        {thefunny.get((6,3), 0) / (col[3]):0.4f}        {thefunny.get((6,4), 0) / (col[4]):0.4f}        {thefunny.get((6,5), 0) / (col[5]):0.4f}        {thefunny.get((6,6), 0) / (col[6]):0.4f}")
print(f"7 | {thefunny.get((7,0), 0) / (col[0]):0.4f}        {thefunny.get((7,1), 0) / (col[1]):0.4f}        {thefunny.get((7,2), 0) / (col[2]):0.4f}        {thefunny.get((7,3), 0) / (col[3]):0.4f}        {thefunny.get((7,4), 0) / (col[4]):0.4f}        {thefunny.get((7,5), 0) / (col[5]):0.4f}        {thefunny.get((7,6), 0) / (col[6]):0.4f}        {thefunny.get((7,7), 0) / (col[7]):0.4f}")
print(f"8 | {thefunny.get((8,0), 0) / (col[0]):0.4f}        {thefunny.get((8,1), 0) / (col[1]):0.4f}        {thefunny.get((8,2), 0) / (col[2]):0.4f}        {thefunny.get((8,3), 0) / (col[3]):0.4f}        {thefunny.get((8,4), 0) / (col[4]):0.4f}        {thefunny.get((8,5), 0) / (col[5]):0.4f}        {thefunny.get((8,6), 0) / (col[6]):0.4f}        {thefunny.get((8,7), 0) / (col[7]):0.4f}        {thefunny.get((8,8), 0) / (col[8]):0.4f}")

print("Conditional PMF of Y given X")
print("y:|   0            1             2             3             4               5              6              7              8")
print("x -----------------------------------------------------------------------------------------------------------------------------------------------")
print(f"0 | {thefunny.get((0,0), 0) / (row[9]):0.4f}")
print(f"1 | {thefunny.get((1,0), 0) / (row[8]):0.4f}        {thefunny.get((1,1), 0) / (row[8]):0.4f}")
print(f"2 | {thefunny.get((2,0), 0) / (row[7]):0.4f}        {thefunny.get((2,1), 0) / (row[7]):0.4f}        {thefunny.get((2,2), 0) / (row[7]):0.4f}")
print(f"3 | {thefunny.get((3,0), 0) / (row[6]):0.4f}        {thefunny.get((3,1), 0) / (row[6]):0.4f}        {thefunny.get((3,2), 0) / (row[6]):0.4f}        {thefunny.get((3,3), 0) / (row[6]):0.4f}")
print(f"4 | {thefunny.get((4,0), 0) / (row[5]):0.4f}        {thefunny.get((4,1), 0) / (row[5]):0.4f}        {thefunny.get((4,2), 0) / (row[5]):0.4f}        {thefunny.get((4,3), 0) / (row[5]):0.4f}        {thefunny.get((4,4), 0) / (row[5]):0.4f}")
print(f"5 | {thefunny.get((5,0), 0) / (row[4]):0.4f}        {thefunny.get((5,1), 0) / (row[4]):0.4f}        {thefunny.get((5,2), 0) / (row[4]):0.4f}        {thefunny.get((5,3), 0) / (row[4]):0.4f}        {thefunny.get((5,4), 0) / (row[4]):0.4f}        {thefunny.get((5,5), 0) / (row[4]):0.4f}")
print(f"6 | {thefunny.get((6,0), 0) / (row[3]):0.4f}        {thefunny.get((6,1), 0) / (row[3]):0.4f}        {thefunny.get((6,2), 0) / (row[3]):0.4f}        {thefunny.get((6,3), 0) / (row[3]):0.4f}        {thefunny.get((6,4), 0) / (row[3]):0.4f}        {thefunny.get((6,5), 0) / (row[3]):0.4f}        {thefunny.get((6,6), 0) / (row[3]):0.4f}")
print(f"7 | {thefunny.get((7,0), 0) / (row[2]):0.4f}        {thefunny.get((7,1), 0) / (row[2]):0.4f}        {thefunny.get((7,2), 0) / (row[2]):0.4f}        {thefunny.get((7,3), 0) / (row[2]):0.4f}        {thefunny.get((7,4), 0) / (row[2]):0.4f}        {thefunny.get((7,5), 0) / (row[2]):0.4f}        {thefunny.get((7,6), 0) / (row[2]):0.4f}        {thefunny.get((7,7), 0) / (row[2]):0.4f}")
print(f"8 | {thefunny.get((8,0), 0) / (row[1]):0.4f}        {thefunny.get((8,1), 0) / (row[1]):0.4f}        {thefunny.get((8,2), 0) / (row[1]):0.4f}        {thefunny.get((8,3), 0) / (row[1]):0.4f}        {thefunny.get((8,4), 0) / (row[1]):0.4f}        {thefunny.get((8,5), 0) / (row[1]):0.4f}        {thefunny.get((8,6), 0) / (row[1]):0.4f}        {thefunny.get((8,7), 0) / (row[1]):0.4f}        {thefunny.get((8,8), 0) / (row[1]):0.4f}")
