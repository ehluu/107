import random

def trial(): # randomly select a ball and then remove it from the sample pool unless the selected ball is a different material than the previous . Repeat until there are no more balls left.
    a = 10 #                   azure balls, let a = 10, 50, 90
    c = 100 - a #              carmine balls, 100 total balls
    p = -2 #                    track if its the first ball being drawn or a dupe
    while(a+c > 0):
        if p == -2:#            if its the first ball being drawn
            i = random.randint(1,a+c)# draw the ball
            if i <= a:#        if its an azure ball
                p = 1#        remember that its an azure ball
                a-=1#          remove the ball from the urn
            else:#             if its a carmine ball
                p = 0#        remember that its a carmine ball
                c -= 1#        remove the carmine ball
        else:#                 if its not the first ball being drawn
            i = random.randint(1,a+c)# draw the ball
            if i <= a:#        if you draw an azure ball
                if p == 1:#   if its a dupe
                    a-=1#      throw the ball out
                else:#         if its not a dupe
                    p = -2#     put the ball back
            else:#             if you draw a carmine ball
                if p == -0:#   if its a dupe
                    c-=1#      throw the ball out
                else:#         if its not a dupe
                    p = -2#     put the ball back
    return p

asdf = 0
for bababooey in range(0,2000):
    asdf += trial()
print(asdf/2000)