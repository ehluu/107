import random

mn = 10000 # idk

def coinflip(rng):
    i = random.randint(1,10)
    if i <= rng:
        return 1
    return 0

def trial(p, q):
    # Flip Coin 1 until the first head appears, counting the number of flips.
    # let 𝑁 be the number of flips in this first sequence.
    n = 0
    while(1):
        ligma = coinflip(p)
        n += 1
        if ligma == 1:
            break

    # Next, perform 𝑁 flips of Coin 2, counting the number of heads. 
    # Let 𝑌 be the number of heads in this second sequence.
    y = 0
    for i in range(n):
        sugma = coinflip(q)
        if sugma == 1:
            y += 1
    return y

hi = [1,2,3,4,5,6,7,8,9]
ohayo = []
# E[Y] and Var[Y]
for pp in hi:
    for qq in hi:
        hey = 0
        hello = 0
        for asdf in range(mn): # 10,000
            sup = trial(pp, qq)
            hey += sup
            hello += sup*sup
        mean = hey/mn
        var = hello/mn - mean*mean
        # print(f"{pp} {qq} {mean:0.4f} {var:0.4f}", end=" ")
        ohayo.append([mean, var])
    # print()

# print means
for bonjour in range(9):
    for konwa in range(9):
        print(f"{ohayo[konwa + bonjour * 9][0]:7.3f}", end=" ")
    print()
print()
# print vars
for hola in range(9):
    for nihao in range(9):
        print(f"{ohayo[nihao + hola * 9][1]:7.3f}", end=" ")
    print()
