import random
rng = 1#                chance of heads
def coinflip():
    i = random.randint(1,10)
    if i <= rng:
        return 1
    return 0

n = 1000#                  n coins
def trial():
    bob = 0
    alice = 0
    for j in range(0, n+1): # bob n + 1
        bob += coinflip()#  number of heads in one trial
    for j in range(0, n): # alice n 
        alice += coinflip()
    if bob > alice:
        return 1
    return 0

def simulation():
    #1000 trials 
    bobh = 0
    for bababooey in range(0,1000):
        bobh += trial()
    return bobh/1000
    # print("relative frequency: ", bobh/1000 )

def experiment():
    gongaga = 0
    m = 100
    for e in range(0,m):
        gongaga += simulation()
    return gongaga/m

print(experiment())